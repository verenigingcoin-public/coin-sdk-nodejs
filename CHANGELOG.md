# Changelog

## Version 1.2.0

Update:

- Updated HMAC encryption to include query parameters since API gateway does not support old method (no query params) anymore.

## Version 1.1.3

Fixed:

- Reset reconnect counter when reconnecting to escape reconnect loops

## Version 1.1.2

Fixed:

- fix handling of pradelayed messages

## Version 1.1.1

Fixed:

- timestamp generation on header messages

## Version 1.1.0

Added:

- Support for Number Portability v3; adds the contract field to porting requests
  
Changed:

- Support Node 14 LTS
- SDK implementation now uses ES modules, but library is still published as CJS
- Deprecated `request` package got replaced with `got`
- Miscellaneous dependency upgrades
