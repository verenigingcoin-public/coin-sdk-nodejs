# COIN SDK for Node.js

[![npm version](https://badge.fury.io/js/%40devops_coin%2Fcoin-sdk.svg)](https://badge.fury.io/js/%40devops_coin%2Fcoin-sdk)
[![CI Status](https://gitlab.com/verenigingcoin-public/coin-sdk-nodejs/badges/master/pipeline.svg)](https://gitlab.com/verenigingcoin-public/coin-sdk-nodejs/-/pipelines/latest)

| Api                                                                   | SDK Version   | Api Version                                          | Changelog                           |
|-----------------------------------------------------------------------|---------------|------------------------------------------------------|-------------------------------------|
| mobile-connect | 1.3.0 +       | [v3](https://api.coin.nl/docs/mobile-connect/v3) | [click](CHANGELOG.md#version-1.x.x) |
| [number-portability](https://coin.nl/en/services/nummerportabiliteit) | 1.1.0 +       | [v3](https://api.coin.nl/docs/number-portability/v3) | [click](CHANGELOG.md#version-1.1.3) |
|                                                                       | 0.0.2 - 1.0.3 | [v1](https://api.coin.nl/docs/number-portability/v1) | -                                   |
| [bundle-switching](https://coin.nl/en/services/overstappen)           | n/a           | n/a                                                  | n/a                                 |

This project contains SDKs for various COIN APIs.
- [Number Portability](sdk/src/number-portability-sdk/README.md)
- For other APIs you can use [Common](sdk/src/common-sdk) to add correct credentials to your requests

To use an SDK, you need to [configure a consumer](https://gitlab.com/verenigingcoin-public/consumer-configuration/-/blob/master/README.md).

## Support
If you need support, feel free to send an email to the [COIN devops team](mailto:devops@coin.nl).
