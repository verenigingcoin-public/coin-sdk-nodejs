const got = require('got');
const NumberPortabilityMessageConsumer = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/api/consumer');
const OffsetPersister = require('./offset-persister');
const MessageListener = require('./message-listener');
const RestApiClient = require('@devops_coin/coin-sdk/common-sdk/rest-api');

// const baseUrl = "https://test-api.coin.nl";
// const baseUrl = "http://localhost:8000";
const baseUrl = "https://dev-api.coin.nl";
// const consumerName = "loadtest-loada";
const consumerName = "API_T_DEMO1";
const privateKeyFile = "keys/demo1-private-key.pem";
const encryptedHmacSecretFile = "keys/demo1-sharedkey.encrypted";
const validPeriodInSeconds = 30;

class StopStreamService extends RestApiClient {

  constructor(consumerName, privateKeyFile = null, encryptedHmacSecretFile = null, validPeriodInSeconds = 30, baseUrl) {
    super(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds);
    this.apiUrl = baseUrl + '/number-portability/v3';
    console.log(this.apiUrl);
  }

  stopStream() {
    const url = `${this.apiUrl}/dossiers/stopstream`;

    return got.get(url, {
      headers: this.generateHeaders('PUT', url),
      responseType: 'json'
    });
  }
}

// eslint-disable-next-line no-unused-vars
class MessageConsumer {
  constructor() {
    this.consumer = new NumberPortabilityMessageConsumer(consumerName, privateKeyFile, encryptedHmacSecretFile, baseUrl, 30, 10, validPeriodInSeconds);
  }

  run() {
    return this.consumer.startConsuming(new MessageListener(), {offsetPersister: new OffsetPersister()});
  }
}

// eslint-disable-next-line no-unused-vars
class MessageConsumerWithInterruptExample {
  constructor() {
    this.consumer = new NumberPortabilityMessageConsumer(consumerName, privateKeyFile, encryptedHmacSecretFile, baseUrl, 30, 10, validPeriodInSeconds);
    this.stopStream = new StopStreamService(consumerName, privateKeyFile, encryptedHmacSecretFile, 30, baseUrl, 10, validPeriodInSeconds);
  }

  run() {
    return this.consumer.startConsuming(
      new MessageListener(() => this.stopStream.stopStream()),
      {offsetPersister: new OffsetPersister()}
    );
  }
}

const sample2 = new MessageConsumer();
// const sample2 = new MessageConsumerWithInterruptExample();
const disposable = sample2.run();

disposable.promise.then(() => {
  console.log("stream closed");
});

setTimeout(() => disposable.close(), 5000);
