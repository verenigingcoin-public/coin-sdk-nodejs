const ActivationServiceNumberMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/ActivationServiceNumberMessage');
const CancelMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/CancelMessage');
const DeactivationMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/DeactivationMessage');
const DeactivationServiceNumberMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/DeactivationServiceNumberMessage');
const EnumActivationNumberMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/EnumActivationNumberMessage');
const EnumActivationOperatorMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/EnumActivationOperatorMessage');
const EnumActivationRangeMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/EnumActivationRangeMessage');
const EnumDeactivationNumberMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/EnumDeactivationNumberMessage');
const EnumDeactivationOperatorMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/EnumDeactivationOperatorMessage');
const EnumDeactivationRangeMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/EnumDeactivationRangeMessage');
const EnumProfileActivationMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/EnumProfileActivationMessage');
const EnumProfileDeactivationMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/EnumProfileDeactivationMessage');
const ErrorFoundMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/ErrorFoundMessage');
const PortingPerformedMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/PortingPerformedMessage');
const PortingRequestMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/PortingRequestMessage');
const PortingRequestAnswerMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/PortingRequestAnswerMessage');
const PortingRequestAnswerDelayedMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/PortingRequestAnswerDelayedMessage');
const RangeActivationMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/RangeActivationMessage');
const RangeDeactivationMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/RangeDeactivationMessage');
const TariffChangeServiceNumberMessage = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/TariffChangeServiceNumberMessage');

class MessageListener {

  constructor(sideEffect) {
    this.sideEffect = sideEffect;
  }

  onActivationsn(message) {
    const activationSnMessage = ActivationServiceNumberMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(activationSnMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onCancel(message) {
    const cancelMessage = CancelMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(cancelMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onDeactivation(message) {
    const deactivationMessage = DeactivationMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(deactivationMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onDeactivationsn(message) {
    const deactivationSnMessage = DeactivationServiceNumberMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(deactivationSnMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onEnumActivationNumber(message) {
    const enumActivationNumberMessage = EnumActivationNumberMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumActivationNumberMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onEnumActivationOperator(message) {
    const enumActivationOperatorMessage = EnumActivationOperatorMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumActivationOperatorMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onEnumActivationRange(message) {
    const enumActivationRangeMessage = EnumActivationRangeMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumActivationRangeMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onEnumDeactivationNumber(message) {
    const enumDeactivationNumberMessage = EnumDeactivationNumberMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumDeactivationNumberMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onEnumDeactivationOperator(message) {
    const enumDeactivationOperatorMessage = EnumDeactivationOperatorMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumDeactivationOperatorMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onEnumDeactivationRange(message) {
    const enumDeactivationRangeMessage = EnumDeactivationRangeMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumDeactivationRangeMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onEnumProfileActivation(message) {
    const enumProfileActivation = EnumProfileActivationMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumProfileActivation)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onEnumProfileDeactivation(message) {
    const enumProfileDeactivation = EnumProfileDeactivationMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(enumProfileDeactivation)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onErrorFound(message) {
    const errorFound = ErrorFoundMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(errorFound)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onPortingPerformed(message) {
    const portingPerformed = PortingPerformedMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(portingPerformed)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onPortingRequest(message) {
    const portingRequest = PortingRequestMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(portingRequest)}`);
    if (this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onPortingRequestAnswer(message) {
    const portingRequestAnswer = PortingRequestAnswerMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(portingRequestAnswer)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onPortingRequestAnswerDelayed(message) {
    const portingRequestAnswerDelayed = PortingRequestAnswerDelayedMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(portingRequestAnswerDelayed)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onRangeActivation(message) {
    const rangeActivationMessage = RangeActivationMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(rangeActivationMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onRangeDeactivation(message) {
    const rangeDeactivationMessage = RangeDeactivationMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(rangeDeactivationMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onTariffChangeSn(message) {
    const tariffChangeServiceNumberMessage = TariffChangeServiceNumberMessage.constructFromObject(JSON.parse(message.data).message);
    console.log(`${message.lastEventId} - ${JSON.stringify(tariffChangeServiceNumberMessage)}`);
    if ( this.sideEffect && typeof this.sideEffect === 'function') this.sideEffect();
  }

  onProcess(message) {
    console.log(`${message.lastEventId} - ${JSON.stringify(message)}`);
  }
}

module.exports = MessageListener;
