const NumberPortabilityService = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/api/service');
const PortingRequestBuilder = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/builder/PortingRequestBuilder');
const PortingRequestAnswerBuilder = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/builder/PortingRequestAnswerBuilder');
const PortingRequestAnswerDelayedBuilder = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/builder/PortingRequestAnswerDelayedBuilder');
const PortingPerformedBuilder = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/builder/PortingPerformedBuilder');
const CancelBuilder = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/builder/CancelBuilder');
const DeactivationBuilder = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/builder/DeactivationBuilder');
const MessageResponse = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/model/MessageResponse');

const validPeriodInSeconds = 30;
const env = require('@devops_coin/coin-sdk/common-sdk/env');

env.init();

const senderNetworkOperator = 'TST01';
const senderServiceProvider = senderNetworkOperator;
const receiverNetworkOperator = 'TST02';
const receiverServiceProvider = receiverNetworkOperator;

// Demo dossier-id as an example, in real world scenario's. The id should be generated an unique (in case of a PortingRequest)
// or the dossier-id should match the received dossier-id (in case of a PortingRequestAnswer)
const dossierId = 'TST01-123456';

// For Demo purposes the next constants are defined to show the working. In real life secanrio's the number ranges must
// match the number range of the donor/recipient and als the timestamp should be generated before the message is sent.
const startNumberRange = "0303800007";
const endNumberRange = "0303800007";

class MessageSender {

  constructor() {
    const baseUrl = env.get("BASE_URL");
    const consumerName = env.get("CONSUMER_NAME");
    const privateKeyFile = env.get("PRIVATE_KEY_FILE");
    const encryptedHmacSecretFile = env.get("SHARED_SECRET_FILE");
    this.service = new NumberPortabilityService(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds, baseUrl);
  }

  sendPortingRequestMessage() {
    const builder = new PortingRequestBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, 'CRDB', null)
      .setDossierId(dossierId)
      .setRecipientNetworkOperator(senderNetworkOperator)
      .addPortingRequestSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    this.service.sendMessage(message).then(function(result) {
      const response = {};
      MessageResponse.constructFromObject(result, response);
      console.log(response.transactionId);
    });
  }

  sendPortingRequestAnswerMessage() {
    const builder = new PortingRequestAnswerBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setBlocking("N")
      .addPortingRequestAnswerSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    this.service.sendMessage(message).then(function(result) {
      const response = {};
      MessageResponse.constructFromObject(result, response);
      console.log(response.transactionId);
    });
  }

  sendPortingRequestAnswerDelayedMessage() {
    const builder = new PortingRequestAnswerDelayedBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setDonorNetworkOperator(senderNetworkOperator)
      .setReasonCode("99")
      .build();

    this.service.sendMessage(message).then(function(result) {
      const response = {};
      MessageResponse.constructFromObject(result, response);
      console.log(response.transactionId);
    });
  }

  sendPortingPerformedMessage() {
    const builder = new PortingPerformedBuilder();
    const message = builder
      .setHeader(senderNetworkOperator, 'ALLO')
      .setDossierId(dossierId)
      .setRecipientNetworkOperator(senderNetworkOperator)
      .setDonorNetworkOperator(receiverNetworkOperator)
      .addPortingPerformedSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    this.service.sendMessage(message).then(function(result) {
      const response = {};
      MessageResponse.constructFromObject(result, response);
      console.log(response.transactionId);
    });
  }

  sendCancelMessage() {
    const builder = new CancelBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setDonorNetworkOperator(senderNetworkOperator)
      .setNote("some additional information")
      .build();

    this.service.sendMessage(message).then(function(result) {
      const response = {};
      MessageResponse.constructFromObject(result, response);
      console.log(response.transactionId);
    });
  }

  sendDeactivationMessage() {
    const builder = new DeactivationBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setOriginalNetworkOperator(receiverNetworkOperator)
      .setCurrentNetworkOperator(senderNetworkOperator)
      .addDeactivationSequence()
      .setNumberSeries(startNumberRange, endNumberRange)
      .finish()
      .build();

    this.service.sendMessage(message).then(function(result) {
      const response = {};
      MessageResponse.constructFromObject(result, response);
      console.log(response.transactionId);
    });
  }
}

const sample1 = new MessageSender();
sample1.sendPortingRequestMessage();
sample1.sendPortingRequestAnswerMessage();
sample1.sendPortingPerformedMessage();
