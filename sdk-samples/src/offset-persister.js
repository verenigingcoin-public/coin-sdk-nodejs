class OffsetPersister {

  constructor() {
    this.persistedOffset = -1;
  }
  
  persistOffset(offset) {
    this.persistedOffset = offset;
  }
}

module.exports = OffsetPersister;
