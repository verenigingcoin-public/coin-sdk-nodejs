const MobileConnectService = require('@devops_coin/coin-sdk/mobile-connect-sdk/messages/v3/api/service');

const {use} = require('chai');
const chaiAsPromised = require('chai-as-promised');

use(chaiAsPromised);

const {
    baseUrl,
    consumerName,
    privateKeyFile,
    encryptedHmacSecretFile
} = require('./index');

describe('mobile connect service', () => {
    const service = new MobileConnectService(consumerName, privateKeyFile, encryptedHmacSecretFile, 30, baseUrl);

    it('send discovery request', async () => {
        const discoveryResponse = await service.sendDiscoveryRequest('123456789');
        console.log(discoveryResponse);
    });
});
