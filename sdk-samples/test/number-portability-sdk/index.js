const env = require('@devops_coin/coin-sdk/common-sdk/env');

env.init();

module.exports = {
  baseUrl: env.get("BASE_URL"),
  consumerName: env.get("CONSUMER_NAME"),
  privateKeyFile: env.get("PRIVATE_KEY_FILE"),
  encryptedHmacSecretFile: env.get("SHARED_SECRET_FILE")
};
