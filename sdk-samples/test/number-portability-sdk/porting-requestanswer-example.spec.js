const PortingRequestAnswerBuilder = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/builder/PortingRequestAnswerBuilder');
const NumberPortabilityService = require('@devops_coin/coin-sdk/number-portability-sdk/messages/v3/api/service');

const {use, expect} = require('chai');
const chaiAsPromised = require('chai-as-promised');

use(chaiAsPromised);

const constants = require('./constants');
const {
  baseUrl,
  consumerName,
  privateKeyFile,
  encryptedHmacSecretFile
} = require('./index');


describe('PortingRequestAnswerExample', () => {

  const service = new NumberPortabilityService(consumerName, privateKeyFile, encryptedHmacSecretFile, 30, baseUrl);

  it('send porting request answer', (done) => {
    const builder = new PortingRequestAnswerBuilder();
    const message = builder
      .setFullHeader(constants.SENDER_NETWORK_OPERATOR, constants.SENDER_SERVICE_PROVIDER, constants.RECEIVER_NETWORK_OPERATOR, constants.RECEIVER_SERVICE_PROVIDER)
      .setDossierId(constants.DOSSIER_ID)
      .setBlocking("N")
      .addPortingRequestAnswerSequence()
      .setFirstPossibleDate(constants.TODAY)
      .setNumberSeries(constants.START_NUMBER_RANGE, constants.END_NUMBER_RANGE)
      .setDonorNetworkOperator(constants.SENDER_NETWORK_OPERATOR)
      .setDonorServiceProvider(constants.SENDER_SERVICE_PROVIDER)
      .finish()
      .build();

    console.log(JSON.stringify(message))

    const result = service.sendMessage(message);
    result.catch(done);
    result.then(obj => {
      expect(obj.transactionId).to.match(constants.REGEX_TRANSACTION_ID);
      done();
    });
  });
});
