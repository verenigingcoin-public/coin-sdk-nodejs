import utils from './utils';
import env from './env';

// Log levels:
//   0 = None
//   1 = Errors only
//   2 = Warnings
//   3 = Info
//   4 or higher = Debug (verbose)

class Logger {

  constructor(name, _level = null) {
    this.name = name.toUpperCase();
    this.level = _level || env.get('LOG_LEVEL');
  }

  error(message) {
    if (this.level > 0) {
      console.error(`${utils.timestamp()} ${this.name} [ERROR] ${message}`);
    }
  }

  warn(message) {
    if (this.level > 1) {
      console.error(`${utils.timestamp()} ${this.name} [WARN] ${message}`);
    }
  }

  info(message) {
    if (this.level > 2) {
      console.log(`${utils.timestamp()} ${this.name} [INFO] ${message}`);
    }
  }

  debug(message) {
    if (this.level > 3) {
      console.log(`${utils.timestamp()} ${this.name} [DEBUG] ${message}`);
    }
  }
}

export default Logger;
