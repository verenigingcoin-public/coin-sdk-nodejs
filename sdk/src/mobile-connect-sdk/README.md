# COIN SDK Nodejs

The Vereniging COIN supplies third parties with an SDK for Node.js that supports secured access to the Mobile Connect Discovery API.

## Installation

First you will need to have the latest version of Node installed on your machine, so execute the following commands:

```
$ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash - 
$ sudo apt-get install -y nodejs
```

Ensure that you have the correct versions of node and npm installed:

```
$ node -v
v20.18.0
```
```
$ npm -v
11.0.0
```

The SDK is available as an NPM package. You can download and install the SDK as follows:

```
$ $ npm install @devops_coin/coin-sdk
```

## Settings

We use the [dotenv](https://github.com/motdotla/dotenv) library for adjusting the application settings.

```
$ cat env.example
BASE_URL=https://test-api.coin.nl
CONSUMER_NAME=<<FILL IN THE CONSUMER NAME>>
```

You need to copy the `env.example` file to `.env` and make needed changes which will be used by the application.

In order to use this settings library, be sure that you add the following at the top of your file:

```
import env from '@devops_coin/coin-sdk/common-sdk/env';
env.init();
```

## Security

See the [jsrsasign](https://github.com/kjur/jsrsasign) library.

## Configure Credentials

For secure access various credentials are required.
- Check [this README](https://gitlab.com/verenigingcoin-public/consumer-configuration/-/blob/master/README.md) to find out how to configure these.
- To summarize, you will need:
    - Consumer name 
    - `private-key.pem` file
    - `sharedkey.encrypted` encrypted (by public key) HMAC secret file

Add the following properties to the .env file

```
# Name of the consumer as configured in: TEST - https://test-portal.coin.nl/iam#/ or  PROD - https://portal.coin.nl/iam#/ 
CONSUMER_NAME=<your-consumer-name>
# Path to the private key file (public key is registerd in the COIN IAM Portal)
PRIVATE_KEY_FILE=path-to/private-key.pem
#Path to encrypted HMAC secret for given consumer as copied from the COIN IAM Portal (links see above)
SHARED_SECRET_FILE=path-to/sharedkey.encrypted
```

## Discovery Request

The Mobile Connect SDK can send the following functional messages:
- Discovery Request

### Sending Request

The sending of a discovery request can be done in the following manner:

```javascript
const MobileConnectService = require('@devops_coin/coin-sdk/mobile-connect-sdk/messages/v3/api/service');

const service = new MobileConnectService(/* Supply credentials */);

// using promises
this.service.sendDiscoveryRequest('1234567890').then(response => {
    console.log(response);
});

// using async await
const response = await this.service.sendDiscoveryRequest('1234567890');
console.log(response);
```


## References 

* [Node.js](https://nodejs.org)
* [Setting up a Node development environment](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/development_environment)
* [Mocha](https://mochajs.org)
* [Chai/expect](https://www.chaijs.com/api/bdd)
* [Minimist](https://github.com/substack/minimist)
* [jsrsasign](https://github.com/kjur/jsrsasign)
* [dotenv](https://github.com/motdotla/dotenv)
