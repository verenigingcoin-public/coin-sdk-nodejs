import RestApiClient from '../../../common-sdk/rest-api';
import got, {HTTPError} from 'got';
import {DiscoveryResponseV3} from '../index.js';
import ErrorBody from '../domain/ErrorBody.js';
import DiscoveryRequestV3 from "../domain/DiscoveryRequestV3.js";

const MOBILE_CONNECT_URL = '/mobile-connect/v3/discovery';

class MobileConnectService extends RestApiClient {

  constructor(consumerName, privateKeyFile = null, encryptedHmacSecretFile = null, validPeriodInSeconds = 30, baseUrl) {
    super(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds);
    this.apiUrl = baseUrl + MOBILE_CONNECT_URL;
  }

  /**
   * Perform a discovery request.
   *
   * @param msisdn Mobile number in the international dialing format either 31600000000, +31600000000 or 0031600000000
   * @param correlationId Correlates a transaction across Mobile Connect components.
   * The value is generated by the Service Provider and must be locally unique.
   * This parameter should be used in conjunction with your operator partner and only for tracing and resolving issues.
   * The recommended format is UUID4.
   * @returns {Promise<DiscoveryResponseV3 | ErrorBody | undefined>} `DiscoveryResponse` on successful request,
   * `undefined` for no results, and `ErrorBody` for errors generated by the Mobile Connect Discovery API.
   */
  sendDiscoveryRequest(msisdn, correlationId) {
    const message = new DiscoveryRequestV3(msisdn, correlationId);
    return got.post(this.apiUrl, {
      json: message,
      headers: this.generateHeaders('POST', this.apiUrl, message),
      responseType: 'json'
    })
        .then(response => DiscoveryResponseV3.constructFromObject(response.body))
        .catch(this.handleError);
  }

  handleError = (err) => {
    if (err instanceof HTTPError) {
      try {
        const errorBody = ErrorBody.constructFromObject(err.response.body);
        if (err.response.statusCode === 404 && this._isMobileConnectError(errorBody)) {
          return;
        }
        return errorBody;
      } catch (e) {
        throw new Error('Unknown response', err.response.body);
      }
    }
    throw err;
  };

  _isMobileConnectError(obj) {
    return obj && obj.error && obj.description;
  }
}

export default MobileConnectService;
