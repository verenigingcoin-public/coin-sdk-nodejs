import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import CancelMessage from '../model/CancelMessage';
import CancelBody from '../model/CancelBody';
import Cancel from '../model/Cancel';

class CancelBuilder extends MessageBuilder {

  constructor() {
    super();
    this.cancel = new Cancel();
  }

  setDossierId(dossierId) {
    this.cancel.dossierid = dossierId;
    return this;
  }

  setNote(note) {
    this.cancel.note = note;
    return this;
  }

  build() {
    super.build();
    this.body = new CancelBody(this.cancel);
    this.message = new CancelMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.cancel);
  }
}

export default CancelBuilder;


