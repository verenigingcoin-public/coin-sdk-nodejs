import DeactivationSeq from '../model/DeactivationSeq';
import DeactivationRepeats from '../model/DeactivationRepeats';
import NumberSeries from '../model/NumberSeries';

class DeactivationSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.deactivationSequence = new DeactivationSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.deactivationSequence.numberseries = numberSeries;

    return this;
  }

  finish() {
    this.parent.deactivation.repeats.push(new DeactivationRepeats(this.deactivationSequence));
    return this.parent;
  }
}

export default DeactivationSequenceBuilder;


