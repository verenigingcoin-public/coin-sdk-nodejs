import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import EnumOperatorSequenceBuilder from './EnumOperatorSequenceBuilder';
import EnumActivationOperatorMessage from '../model/EnumActivationOperatorMessage';
import EnumActivationOperatorBody from '../model/EnumActivationOperatorBody';
import EnumOperatorContent from '../model/EnumOperatorContent';

class EnumActivationOperatorBuilder extends MessageBuilder {

  constructor() {
    super();
    this.enumcontent = new EnumOperatorContent();
    this.enumcontent.repeats = [];
  }

  setDossierId(dossierId) {
    this.enumcontent.dossierid = dossierId;
    return this;
  }

  setCurrentNetworkOperator(currentNetworkOperator) {
    this.enumcontent.currentnetworkoperator = currentNetworkOperator;
    return this;
  }

  setTypeOfNumber(typeOfNumber) {
    this.enumcontent.typeofnumber = typeOfNumber;
    return this;
  }

  addEnumOperatorSequence() {
    return new EnumOperatorSequenceBuilder(this);
  }

  build() {
    super.build();
    this.body = new EnumActivationOperatorBody(this.enumcontent);
    this.message = new EnumActivationOperatorMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.enumactivationoperator);
  }
}

export default EnumActivationOperatorBuilder;


