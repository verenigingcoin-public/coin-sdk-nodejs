import EnumOperatorSeq from '../model/EnumOperatorSeq';
import EnumOperatorRepeats from '../model/EnumOperatorRepeats';

class EnumOperatorSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.enumOperatorSequence = new EnumOperatorSeq();
  }

  setProfileId(profileId) {
    this.enumOperatorSequence.profileid = profileId;
    return this;
  }

  setDefaultService(defaultService) {
    this.enumOperatorSequence.defaultservice = defaultService;
    return this;
  }

  finish() {
    this.parent.enumcontent.repeats.push(new EnumOperatorRepeats(this.enumOperatorSequence));
    return this.parent;
  }

}

export default EnumOperatorSequenceBuilder;
