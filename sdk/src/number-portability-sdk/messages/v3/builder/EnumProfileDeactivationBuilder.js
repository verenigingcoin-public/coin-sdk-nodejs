import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import EnumProfileDeactivation from '../model/EnumProfileDeactivation';
import EnumProfileDeactivationBody from '../model/EnumProfileDeactivationBody';
import EnumProfileDeactivationMessage from '../model/EnumProfileDeactivationMessage';

class EnumProfileDeactivationBuilder extends MessageBuilder {

  constructor() {
    super();
    this.enumprofiledeactivation = new EnumProfileDeactivation();
  }

  setDossierId(dossierId) {
    this.enumprofiledeactivation.dossierid = dossierId;
    return this;
  }

  setCurrentNetworkOperator(currentNetworkOperator) {
    this.enumprofiledeactivation.currentnetworkoperator = currentNetworkOperator;
    return this;
  }

  setTypeOfNumber(typeOfNumber) {
    this.enumprofiledeactivation.typeofnumber = typeOfNumber;
    return this;
  }

  setProfileId(profileId) {
    this.enumprofiledeactivation.profileid = profileId;
    return this;
  }

  build() {
    super.build();

    this.body = new EnumProfileDeactivationBody(this.enumprofiledeactivation);
    this.message = new EnumProfileDeactivationMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.enumprofiledeactivation);
  }
}

export default EnumProfileDeactivationBuilder;
