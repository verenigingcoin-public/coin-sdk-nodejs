import EnumProfileSeq from '../model/EnumProfileSeq';
import EnumRepeats from '../model/EnumRepeats';

class EnumRepeatsBuilder {

  setProfileIds(profileIds) {
    this.profileIds = profileIds;
    return this;
  }

  build() {
    const enumRepeats = [];
    this.profileIds.forEach(function(item) {
      const enumProfileSeq = new EnumProfileSeq();
      enumProfileSeq.profileid = item;
      enumRepeats.push(new EnumRepeats(enumProfileSeq));
    });
    return enumRepeats;
  }
}

export default EnumRepeatsBuilder;


