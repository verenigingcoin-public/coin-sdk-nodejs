import Sender from '../model/Sender';
import Receiver from '../model/Receiver';
import Header from '../model/Header';
import utils from '../../../../common-sdk/utils';

class HeaderBuilder {

  setHeader(senderNetworkOperator, receiverNetworkOperator) {
    this.senderNetworkOperator = senderNetworkOperator;
    this.receiverNetworkOperator = receiverNetworkOperator;
    return this;
  }

  setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider) {
    this.senderNetworkOperator = senderNetworkOperator;
    this.senderServiceProvider = senderServiceProvider;

    this.receiverNetworkOperator = receiverNetworkOperator;
    this.receiverServiceProvider = receiverServiceProvider;
    return this;
  }

  build() {
    if (this.senderServiceProvider) {
      this.sender = Sender.constructFromObject({"networkoperator": this.senderNetworkOperator, "serviceprovider": this.senderServiceProvider});
    } else {
      this.sender = new Sender(this.senderNetworkOperator);
    }

    if (this.receiverServiceProvider) {
      this.receiver = Receiver.constructFromObject({"networkoperator": this.receiverNetworkOperator, "serviceprovider": this.receiverServiceProvider});
    } else {
      this.receiver = new Receiver(this.receiverNetworkOperator);
    }

    const today = new Date();
    const timestamp = utils.formatDate(today)

    this.header = new Header(
      this.receiver,
      this.sender,
      timestamp);
  }
}


export default HeaderBuilder;
