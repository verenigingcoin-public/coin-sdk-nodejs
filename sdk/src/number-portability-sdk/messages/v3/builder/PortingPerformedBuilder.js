import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import PortingPerformedMessage from '../model/PortingPerformedMessage';
import PortingPerformedBody from '../model/PortingPerformedBody';
import PortingPerformed from '../model/PortingPerformed';
import PortingPerformedSequenceBuilder from './PortingPerformedSequenceBuilder';

class PortingPerformedBuilder extends MessageBuilder {

  constructor() {
    super();
    this.portingPerformed = new PortingPerformed();
    this.portingPerformed.repeats = [];
  }

  setDossierId(dossierId) {
    this.portingPerformed.dossierid = dossierId;
    return this;
  }

  setRecipientNetworkOperator(recipientNetworkOperator) {
    this.portingPerformed.recipientnetworkoperator = recipientNetworkOperator;
    return this;
  }

  setDonorNetworkOperator(donorNetworkOperator) {
    this.portingPerformed.donornetworkoperator = donorNetworkOperator;
    return this;
  }

  setActualDateTime(actualDateTime) {
    this.portingPerformed.actualdatetime = actualDateTime;
    return this;
  }

  setBatchPorting(batchPorting) {
    this.portingPerformed.batchporting = batchPorting;
    return this;
  }

  addPortingPerformedSequence() {
    return new PortingPerformedSequenceBuilder(this);
  }

  build() {
    super.build();

    this.body = new PortingPerformedBody(this.portingPerformed);
    this.message = new PortingPerformedMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.portingperformed);
  }
}

export default PortingPerformedBuilder;
