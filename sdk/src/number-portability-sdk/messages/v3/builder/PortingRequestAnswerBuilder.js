import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import PortingRequestAnswerMessage from '../model/PortingRequestAnswerMessage';
import PortingRequestAnswerBody from '../model/PortingRequestAnswerBody';
import PortingRequestAnswer from '../model/PortingRequestAnswer';
import PortingRequestAnswerSequenceBuilder from './PortingRequestAnswerSequenceBuilder';

class PortingRequestAnswerBuilder extends MessageBuilder {

  constructor() {
    super();
    this.portingrequestanswer = new PortingRequestAnswer();
    this.portingrequestanswer.repeats = [];
  }

  setDossierId(dossierId) {
    this.portingrequestanswer.dossierid = dossierId;
    return this;
  }

  setBlocking(blocking) {
    this.portingrequestanswer.blocking = blocking;
    return this;
  }

  addPortingRequestAnswerSequence() {
    return new PortingRequestAnswerSequenceBuilder(this);
  }

  build() {
    super.build();

    this.body = new PortingRequestAnswerBody(this.portingrequestanswer);
    this.message = new PortingRequestAnswerMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.portingrequestanswer);
  }
}

export default PortingRequestAnswerBuilder;
