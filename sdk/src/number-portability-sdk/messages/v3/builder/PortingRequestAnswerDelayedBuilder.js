import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import PortingRequestAnswerDelayedMessage from '../model/PortingRequestAnswerDelayedMessage';
import PortingRequestAnswerDelayedBody from '../model/PortingRequestAnswerDelayedBody';
import PortingRequestAnswerDelayed from '../model/PortingRequestAnswerDelayed';

class PortingRequestAnswerDelayedBuilder extends MessageBuilder {

  constructor() {
    super();
    this.portingrequestanswerdelayed = new PortingRequestAnswerDelayed();
  }

  setDossierId(dossierId) {
    this.portingrequestanswerdelayed.dossierid = dossierId;
    return this;
  }

  setDonorNetworkOperator(donorNetworkOperator) {
    this.portingrequestanswerdelayed.donornetworkoperator = donorNetworkOperator;
    return this;
  }

  setDonorServiceProvider(donorServiceProvider) {
    this.portingrequestanswerdelayed.donorserviceprovider = donorServiceProvider;
    return this;
  }

  setAnswerDueDateTime(answerDueDateTime) {
    this.portingrequestanswerdelayed.answerduedatetime = answerDueDateTime;
    return this;
  }

  setReasonCode(reasonCode) {
    this.portingrequestanswerdelayed.reasoncode = reasonCode;
    return this;
  }

  build() {
    super.build();
    this.body = new PortingRequestAnswerDelayedBody(this.portingrequestanswerdelayed);
    this.message = new PortingRequestAnswerDelayedMessage(this.header, this.body);
    return new Message({"message":this.message}, MessageTypeEnum.portingrequestanswerdelayed);
  }
}

export default PortingRequestAnswerDelayedBuilder;
