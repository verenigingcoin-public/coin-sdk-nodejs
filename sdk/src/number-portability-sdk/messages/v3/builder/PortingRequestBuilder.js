import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import PortingRequestMessage from '../model/PortingRequestMessage';
import PortingRequestBody from '../model/PortingRequestBody';
import PortingRequest from '../model/PortingRequest';
import CustomerInfo from '../model/CustomerInfo';
import PortingRequestSequenceBuilder from './PortingRequestSequenceBuilder';

class PortingRequestBuilder extends MessageBuilder {

  constructor() {
    super();
    this.portingrequest = new PortingRequest();
    this.portingrequest.repeats = [];
  }

  setDossierId(dossierId) {
    this.portingrequest.dossierid = dossierId;
    return this;
  }

  setContract(contract) {
    this.portingrequest.contract = contract;
    return this;
  }

  setRecipientNetworkOperator(recipientNetworkOperator) {
    this.portingrequest.recipientnetworkoperator = recipientNetworkOperator;
    return this;
  }

  setRecipientServiceProvider(recipientServiceProvider) {
    this.portingrequest.recipientserviceprovider = recipientServiceProvider;
    return this;
  }

  setCustomerInfo(lastname, companyname, housenr, housenrext, postcode, customerid) {
    this.portingrequest.customerinfo = new CustomerInfo();
    this.portingrequest.customerinfo.lastname = lastname;
    this.portingrequest.customerinfo.companyname = companyname;
    this.portingrequest.customerinfo.housenr = housenr;
    this.portingrequest.customerinfo.housenrext = housenrext;
    this.portingrequest.customerinfo.postcode = postcode;
    this.portingrequest.customerinfo.customerid = customerid;

    return this;
  }

  addPortingRequestSequence() {
    return new PortingRequestSequenceBuilder(this);
  }

  build() {
    super.build();

    this.body = new PortingRequestBody(this.portingrequest);
    this.message = new PortingRequestMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.portingrequest);
  }
}

export default PortingRequestBuilder;
