import PortingRequestRepeats from '../model/PortingPerformedRepeats';
import PortingRequestSeq from '../model/PortingPerformedSeq';
import NumberSeries from '../model/NumberSeries';
import EnumRepeatsBuilder from './EnumRepeatsBuilder';

class PortingRequestSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.portingrequestseq = new PortingRequestSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.portingrequestseq.numberseries = numberSeries;

    return this;
  }

  setProfileIds(profileIds) {
    const enumRepeats = new EnumRepeatsBuilder();
    enumRepeats.setProfileIds(profileIds);
    this.portingrequestseq.repeats = enumRepeats.build();
    return this;
  }

  finish() {
    this.parent.portingrequest.repeats.push(new PortingRequestRepeats(this.portingrequestseq));
    return this.parent;
  }
}

export default PortingRequestSequenceBuilder;


