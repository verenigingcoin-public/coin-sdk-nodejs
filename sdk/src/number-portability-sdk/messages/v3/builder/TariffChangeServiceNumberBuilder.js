import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import TariffChangeServiceNumber from '../model/TariffChangeServiceNumber';
import TariffChangeServiceNumberBody from '../model/TariffChangeServiceNumberBody';
import TariffChangeServiceNumberMessage from '../model/TariffChangeServiceNumberMessage';
import TariffChangeServiceNumberSequenceBuilder from './TariffChangeServiceNumberSequenceBuilder';

class TariffChangeServiceNumberBuilder extends MessageBuilder {

  constructor() {
    super();
    this.tariffchangesn = new TariffChangeServiceNumber();
    this.tariffchangesn.repeats = [];
  }

  setDossierId(dossierId) {
    this.tariffchangesn.dossierid = dossierId;
    return this;
  }

  setPlatformProvider(platformProvider) {
    this.tariffchangesn.platformprovider = platformProvider;
    return this;
  }

  setPlannedDateTime(plannedDateTime) {
    this.tariffchangesn.planneddatetime = plannedDateTime;
    return this;
  }

  addTariffChangeServiceNumberSequence() {
    return new TariffChangeServiceNumberSequenceBuilder(this);
  }

  build() {
    super.build();

    this.body = new TariffChangeServiceNumberBody(this.tariffchangesn);
    this.message = new TariffChangeServiceNumberMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.tariffchangesn);
  }
}

export default TariffChangeServiceNumberBuilder;
