export default class Message {

  constructor(message, messageType) {
    this.message = message;
    this.messageType = messageType;
  }

  getMessage() {
    return this.message;
  }

  getMessageType() {
    return this.messageType;
  }
}
