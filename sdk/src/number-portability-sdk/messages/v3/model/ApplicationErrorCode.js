/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import NumberSeries from './NumberSeries';
import TariffInfo from './TariffInfo';

/**
* The ApplicationErrorCode model module.
* @module model/ApplicationErrorCode
* @version 3.0.0
*/
export default class ApplicationErrorCode {
    /**
    * Constructs a new <code>ApplicationErrorCode</code>.
    * @alias module:model/ApplicationErrorCode
    * @class
    * @param numberseries {module:model/NumberSeries}
    * @param tariffinfo {module:model/TariffInfo}
    * @param pop {String}
    */

    constructor(numberseries, tariffinfo, pop) {


        this['numberseries'] = numberseries;
        this['tariffinfo'] = tariffinfo;
        this['pop'] = pop;

    }

    /**
    * Constructs a <code>ApplicationErrorCode</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/ApplicationErrorCode} obj Optional instance to populate.
    * @return {module:model/ApplicationErrorCode} The populated <code>ApplicationErrorCode</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ApplicationErrorCode();


            if (data.hasOwnProperty('numberseries')) {
                obj['numberseries'] = NumberSeries.constructFromObject(data['numberseries']);
            }
            if (data.hasOwnProperty('tariffinfo')) {
                obj['tariffinfo'] = TariffInfo.constructFromObject(data['tariffinfo']);
            }
            if (data.hasOwnProperty('pop')) {
                obj['pop'] = ApiClient.convertToType(data['pop'], 'String');
            }
        }
        return obj;
    }

    /**
    * @member {module:model/NumberSeries} numberseries
    */
    'numberseries' = undefined;
    /**
    * @member {module:model/TariffInfo} tariffinfo
    */
    'tariffinfo' = undefined;
    /**
    * @member {String} pop
    */
    'pop' = undefined;




}
