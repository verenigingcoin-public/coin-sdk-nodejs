/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import DeactivationServiceNumberMessage from './DeactivationServiceNumberMessage';

/**
* The DeactivationServiceNumberEnvelope model module.
* @module model/DeactivationServiceNumberEnvelope
* @version 3.0.0
*/
export default class DeactivationServiceNumberEnvelope {
    /**
    * Constructs a new <code>DeactivationServiceNumberEnvelope</code>.
    * @alias module:model/DeactivationServiceNumberEnvelope
    * @class
    * @param message {module:model/DeactivationServiceNumberMessage}
    */

    constructor(message) {


        this['message'] = message;

    }

    /**
    * Constructs a <code>DeactivationServiceNumberEnvelope</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/DeactivationServiceNumberEnvelope} obj Optional instance to populate.
    * @return {module:model/DeactivationServiceNumberEnvelope} The populated <code>DeactivationServiceNumberEnvelope</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new DeactivationServiceNumberEnvelope();


            if (data.hasOwnProperty('message')) {
                obj['message'] = DeactivationServiceNumberMessage.constructFromObject(data['message']);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/DeactivationServiceNumberMessage} message
    */
    'message' = undefined;




}
