/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import EnumActivationNumberMessage from './EnumActivationNumberMessage';

/**
* The EnumActivationNumberEnvelope model module.
* @module model/EnumActivationNumberEnvelope
* @version 3.0.0
*/
export default class EnumActivationNumberEnvelope {
    /**
    * Constructs a new <code>EnumActivationNumberEnvelope</code>.
    * @alias module:model/EnumActivationNumberEnvelope
    * @class
    * @param message {module:model/EnumActivationNumberMessage}
    */

    constructor(message) {


        this['message'] = message;

    }

    /**
    * Constructs a <code>EnumActivationNumberEnvelope</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/EnumActivationNumberEnvelope} obj Optional instance to populate.
    * @return {module:model/EnumActivationNumberEnvelope} The populated <code>EnumActivationNumberEnvelope</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new EnumActivationNumberEnvelope();


            if (data.hasOwnProperty('message')) {
                obj['message'] = EnumActivationNumberMessage.constructFromObject(data['message']);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/EnumActivationNumberMessage} message
    */
    'message' = undefined;




}
