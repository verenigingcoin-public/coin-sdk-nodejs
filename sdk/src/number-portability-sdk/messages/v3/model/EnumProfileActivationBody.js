/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import EnumProfileActivation from './EnumProfileActivation';

/**
* The EnumProfileActivationBody model module.
* @module model/EnumProfileActivationBody
* @version 3.0.0
*/
export default class EnumProfileActivationBody {
    /**
    * Constructs a new <code>EnumProfileActivationBody</code>.
    * @alias module:model/EnumProfileActivationBody
    * @class
    * @param enumprofileactivation {module:model/EnumProfileActivation}
    */

    constructor(enumprofileactivation) {


        this['enumprofileactivation'] = enumprofileactivation;

    }

    /**
    * Constructs a <code>EnumProfileActivationBody</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/EnumProfileActivationBody} obj Optional instance to populate.
    * @return {module:model/EnumProfileActivationBody} The populated <code>EnumProfileActivationBody</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new EnumProfileActivationBody();


            if (data.hasOwnProperty('enumprofileactivation')) {
                obj['enumprofileactivation'] = EnumProfileActivation.constructFromObject(data['enumprofileactivation']);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/EnumProfileActivation} enumprofileactivation
    */
    'enumprofileactivation' = undefined;




}
