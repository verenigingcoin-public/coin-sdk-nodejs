/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import ErrorFoundMessage from './ErrorFoundMessage';

/**
* The ErrorFoundEnvelope model module.
* @module model/ErrorFoundEnvelope
* @version 3.0.0
*/
export default class ErrorFoundEnvelope {
    /**
    * Constructs a new <code>ErrorFoundEnvelope</code>.
    * @alias module:model/ErrorFoundEnvelope
    * @class
    * @param message {module:model/ErrorFoundMessage}
    */

    constructor(message) {


        this['message'] = message;

    }

    /**
    * Constructs a <code>ErrorFoundEnvelope</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/ErrorFoundEnvelope} obj Optional instance to populate.
    * @return {module:model/ErrorFoundEnvelope} The populated <code>ErrorFoundEnvelope</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ErrorFoundEnvelope();


            if (data.hasOwnProperty('message')) {
                obj['message'] = ErrorFoundMessage.constructFromObject(data['message']);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/ErrorFoundMessage} message
    */
    'message' = undefined;




}
