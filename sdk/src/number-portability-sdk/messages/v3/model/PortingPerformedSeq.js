/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import EnumRepeats from './EnumRepeats';
import NumberSeries from './NumberSeries';

/**
* The PortingPerformedSeq model module.
* @module model/PortingPerformedSeq
* @version 3.0.0
*/
export default class PortingPerformedSeq {
    /**
    * Constructs a new <code>PortingPerformedSeq</code>.
    * @alias module:model/PortingPerformedSeq
    * @class
    * @param numberseries {module:model/NumberSeries}
    */

    constructor(numberseries) {


        this['numberseries'] = numberseries;

    }

    /**
    * Constructs a <code>PortingPerformedSeq</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/PortingPerformedSeq} obj Optional instance to populate.
    * @return {module:model/PortingPerformedSeq} The populated <code>PortingPerformedSeq</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new PortingPerformedSeq();


            if (data.hasOwnProperty('numberseries')) {
                obj['numberseries'] = NumberSeries.constructFromObject(data['numberseries']);
            }
            if (data.hasOwnProperty('backporting')) {
                obj['backporting'] = ApiClient.convertToType(data['backporting'], 'String');
            }
            if (data.hasOwnProperty('pop')) {
                obj['pop'] = ApiClient.convertToType(data['pop'], 'String');
            }
            if (data.hasOwnProperty('repeats')) {
                obj['repeats'] = ApiClient.convertToType(data['repeats'], [EnumRepeats]);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/NumberSeries} numberseries
    */
    'numberseries' = undefined;
    /**
    * @member {String} backporting
    */
    'backporting' = undefined;
    /**
    * @member {String} pop
    */
    'pop' = undefined;
    /**
    * @member {Array.<module:model/EnumRepeats>} repeats
    */
    'repeats' = undefined;




}
