/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import PortingRequestAnswerDelayed from './PortingRequestAnswerDelayed';

/**
* The PortingRequestAnswerDelayedBody model module.
* @module model/PortingRequestAnswerDelayedBody
* @version 3.0.0
*/
export default class PortingRequestAnswerDelayedBody {
    /**
    * Constructs a new <code>PortingRequestAnswerDelayedBody</code>.
    * @alias module:model/PortingRequestAnswerDelayedBody
    * @class
    * @param pradelayed {module:model/PortingRequestAnswerDelayed}
    */

    constructor(pradelayed) {


        this['pradelayed'] = pradelayed;

    }

    /**
    * Constructs a <code>PortingRequestAnswerDelayedBody</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/PortingRequestAnswerDelayedBody} obj Optional instance to populate.
    * @return {module:model/PortingRequestAnswerDelayedBody} The populated <code>PortingRequestAnswerDelayedBody</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new PortingRequestAnswerDelayedBody();


            if (data.hasOwnProperty('pradelayed')) {
                obj['pradelayed'] = PortingRequestAnswerDelayed.constructFromObject(data['pradelayed']);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/PortingRequestAnswerDelayed} pradelayed
    */
    'pradelayed' = undefined;




}
