/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import PortingRequestAnswerMessage from './PortingRequestAnswerMessage';

/**
* The PortingRequestAnswerEnvelope model module.
* @module model/PortingRequestAnswerEnvelope
* @version 3.0.0
*/
export default class PortingRequestAnswerEnvelope {
    /**
    * Constructs a new <code>PortingRequestAnswerEnvelope</code>.
    * @alias module:model/PortingRequestAnswerEnvelope
    * @class
    * @param message {module:model/PortingRequestAnswerMessage}
    */

    constructor(message) {


        this['message'] = message;

    }

    /**
    * Constructs a <code>PortingRequestAnswerEnvelope</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/PortingRequestAnswerEnvelope} obj Optional instance to populate.
    * @return {module:model/PortingRequestAnswerEnvelope} The populated <code>PortingRequestAnswerEnvelope</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new PortingRequestAnswerEnvelope();


            if (data.hasOwnProperty('message')) {
                obj['message'] = PortingRequestAnswerMessage.constructFromObject(data['message']);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/PortingRequestAnswerMessage} message
    */
    'message' = undefined;




}
