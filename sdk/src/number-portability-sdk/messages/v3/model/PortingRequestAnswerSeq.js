/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import NumberSeries from './NumberSeries';

/**
* The PortingRequestAnswerSeq model module.
* @module model/PortingRequestAnswerSeq
* @version 3.0.0
*/
export default class PortingRequestAnswerSeq {
    /**
    * Constructs a new <code>PortingRequestAnswerSeq</code>.
    * @alias module:model/PortingRequestAnswerSeq
    * @class
    * @param numberseries {module:model/NumberSeries}
    */

    constructor(numberseries) {


        this['numberseries'] = numberseries;

    }

    /**
    * Constructs a <code>PortingRequestAnswerSeq</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/PortingRequestAnswerSeq} obj Optional instance to populate.
    * @return {module:model/PortingRequestAnswerSeq} The populated <code>PortingRequestAnswerSeq</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new PortingRequestAnswerSeq();


            if (data.hasOwnProperty('numberseries')) {
                obj['numberseries'] = NumberSeries.constructFromObject(data['numberseries']);
            }
            if (data.hasOwnProperty('blockingcode')) {
                obj['blockingcode'] = ApiClient.convertToType(data['blockingcode'], 'String');
            }
            if (data.hasOwnProperty('firstpossibledate')) {
                obj['firstpossibledate'] = ApiClient.convertToType(data['firstpossibledate'], 'String');
            }
            if (data.hasOwnProperty('note')) {
                obj['note'] = ApiClient.convertToType(data['note'], 'String');
            }
            if (data.hasOwnProperty('donornetworkoperator')) {
                obj['donornetworkoperator'] = ApiClient.convertToType(data['donornetworkoperator'], 'String');
            }
            if (data.hasOwnProperty('donorserviceprovider')) {
                obj['donorserviceprovider'] = ApiClient.convertToType(data['donorserviceprovider'], 'String');
            }
        }
        return obj;
    }

    /**
    * @member {module:model/NumberSeries} numberseries
    */
    'numberseries' = undefined;
    /**
    * @member {String} blockingcode
    */
    'blockingcode' = undefined;
    /**
    * @member {String} firstpossibledate
    */
    'firstpossibledate' = undefined;
    /**
    * @member {String} note
    */
    'note' = undefined;
    /**
    * @member {String} donornetworkoperator
    */
    'donornetworkoperator' = undefined;
    /**
    * @member {String} donorserviceprovider
    */
    'donorserviceprovider' = undefined;




}
