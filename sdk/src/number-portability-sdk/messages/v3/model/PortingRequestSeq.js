/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import EnumRepeats from './EnumRepeats';
import NumberSeries from './NumberSeries';

/**
* The PortingRequestSeq model module.
* @module model/PortingRequestSeq
* @version 3.0.0
*/
export default class PortingRequestSeq {
    /**
    * Constructs a new <code>PortingRequestSeq</code>.
    * @alias module:model/PortingRequestSeq
    * @class
    * @param numberseries {module:model/NumberSeries}
    */

    constructor(numberseries) {


        this['numberseries'] = numberseries;

    }

    /**
    * Constructs a <code>PortingRequestSeq</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/PortingRequestSeq} obj Optional instance to populate.
    * @return {module:model/PortingRequestSeq} The populated <code>PortingRequestSeq</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new PortingRequestSeq();


            if (data.hasOwnProperty('numberseries')) {
                obj['numberseries'] = NumberSeries.constructFromObject(data['numberseries']);
            }
            if (data.hasOwnProperty('repeats')) {
                obj['repeats'] = ApiClient.convertToType(data['repeats'], [EnumRepeats]);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/NumberSeries} numberseries
    */
    'numberseries' = undefined;
    /**
    * @member {Array.<module:model/EnumRepeats>} repeats
    */
    'repeats' = undefined;




}
