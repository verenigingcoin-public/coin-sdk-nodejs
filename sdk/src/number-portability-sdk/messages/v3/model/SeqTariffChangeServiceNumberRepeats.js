/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import TariffChangeServiceNumberRepeats from './TariffChangeServiceNumberRepeats';

/**
* The SeqTariffChangeServiceNumberRepeats model module.
* @module model/SeqTariffChangeServiceNumberRepeats
* @version 3.0.0
*/
export default class SeqTariffChangeServiceNumberRepeats extends Array {
    /**
    * Constructs a new <code>SeqTariffChangeServiceNumberRepeats</code>.
    * @alias module:model/SeqTariffChangeServiceNumberRepeats
    * @class
    * @extends Array
    */

    constructor() {
        super();


        return this;
    }

    /**
    * Constructs a <code>SeqTariffChangeServiceNumberRepeats</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/SeqTariffChangeServiceNumberRepeats} obj Optional instance to populate.
    * @return {module:model/SeqTariffChangeServiceNumberRepeats} The populated <code>SeqTariffChangeServiceNumberRepeats</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new SeqTariffChangeServiceNumberRepeats();
            ApiClient.constructFromObject(data, obj, 'TariffChangeServiceNumberRepeats');


        }
        return obj;
    }





}
