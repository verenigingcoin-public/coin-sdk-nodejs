import env from '../src/common-sdk/env';

export const baseUrl = env.get('BASE_URL');
export const privateKeyFile = './keys/private-key.pem';
export const encryptedHmacSecretFile = './keys/sharedkey.encrypted';
