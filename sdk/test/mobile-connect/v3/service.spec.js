import MobileConnectService from '../../../src/mobile-connect-sdk/v3/api/service';
import {DiscoveryResponseV3, ErrorBody} from '../../../src/mobile-connect-sdk/v3';
import {baseUrl, encryptedHmacSecretFile, privateKeyFile} from '../../index';
import {expect} from "chai";

const consumerName = 'loadtest-loada';
const validPeriodInSeconds = 30;

describe('mobile connect service', () => {
    const service = new MobileConnectService(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds, baseUrl);

    it('should send discovery request', async () => {
       const discoveryResponse = await service.sendDiscoveryRequest('123456789');
       expect(discoveryResponse).to.not.be.undefined;
       expect(discoveryResponse).to.be.an.instanceOf(DiscoveryResponseV3);
       expect(discoveryResponse.supportedServices).to.not.be.empty;
       expect(discoveryResponse.networkOperatorCode).to.not.be.undefined;
       expect(discoveryResponse.clientId).to.not.be.undefined;
       expect(discoveryResponse.clientSecret).to.not.be.undefined;
    });

    it('should handle discovery request with no result', async () => {
        const discoveryResponse = await service.sendDiscoveryRequest('123456789', '404');
        expect(discoveryResponse).to.be.undefined;
    });

    it('should handle discovery request error', async () => {
        const discoveryResponse = await service.sendDiscoveryRequest('123456789', '403');
        expect(discoveryResponse).to.not.be.undefined;
        expect(discoveryResponse).to.be.an.instanceOf(ErrorBody);
    });
});
