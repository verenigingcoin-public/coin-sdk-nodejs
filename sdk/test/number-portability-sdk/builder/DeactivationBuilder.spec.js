import expect from './common/expect';

import DeactivationBuilder from '../../../src/number-portability-sdk/messages/v3/builder/DeactivationBuilder';
import MessageTypeEnum from '../../../src/number-portability-sdk/messages/v3/messagetype.js';

import results from './results/DeactivationMessage';
import buildMessage from './messages/DeactivationMessage';
import {removeUndefined} from "../../common-sdk/utils";

function fixResult(result, actual) {
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('DeactivationBuilder', () => {
  it('message should be of type activationsn', () => {
    const builder = new DeactivationBuilder();
    const message = buildMessage(0, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.deactivation);
  });

  it('message should have the correct structure', () => {
    const offset = 0;
    const builder = new DeactivationBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = removeUndefined(message.getMessage());

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
