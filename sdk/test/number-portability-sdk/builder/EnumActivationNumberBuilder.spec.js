import expect from './common/expect';

import EnumActivationNumberBuilder from '../../../src/number-portability-sdk/messages/v3/builder/EnumActivationNumberBuilder';
import MessageTypeEnum from '../../../src/number-portability-sdk/messages/v3/messagetype.js';

describe('EnumAcativationNumberBuilder', () => {
  xit('message should be of type enumactivationnumber', () => {
    const result = '{"message":{"header":{"receiver":{"networkoperator":"DEMO2"},"sender":{"networkoperator":"DEMO1"}},"body":{"enumactivationnumber":{"dossierid":"DEMO1-123456","currentnetworkoperator":"DEMO1","typeofnumber":"X","repeats":[{"seq":{"numberseries":{"start":"0123456789","end":"0987654321"},"repeats":[{"seq":{"profileid":"PROF1"}},{"seq":{"profileid":"PROF2"}}]}}]}}}}';
    const builder = new EnumActivationNumberBuilder();
    const message = builder
      .setHeader("DEMO1", "DEMO2")
      .setDossierId("DEMO1-123456")
      .setCurrentNetworkOperator("DEMO1")
      .setTypeOfNumber('X')
      .addEnumNumberSequence()
      .setNumberSeries("0123456789", "0987654321")
      .setProfileIds(['PROF1', 'PROF2'])
      .finish()
      .build();

    expect(message.getMessageType()).to.equal(MessageTypeEnum.enumactivationnumber);
    expect(JSON.stringify(message.getMessage())).to.equal(result);
  });
});
