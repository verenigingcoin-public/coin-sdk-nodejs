import expect from './common/expect';

import EnumProfileActivationBuilder from '../../../src/number-portability-sdk/messages/v3/builder/EnumProfileActivationBuilder';
import MessageTypeEnum from '../../../src/number-portability-sdk/messages/v3/messagetype';

describe('EnumProfileActivationBuilder', () => {
  xit('message should be of type enumprofileactivation', () => {
    const result = '{"message":{"header":{"receiver":{"networkoperator":"DEMO2"},"sender":{"networkoperator":"DEMO1"}},"body":{"enumprofileactivation":{"dossierid":"DEMO1-123456","currentnetworkoperator":"currentNetworkOperator","typeofnumber":"typeOfNumber","scope":"scope","profileid":"profileId","ttl":"ttl","dnsclass":"dnsClass","rectype":"recType","order":"order","preference":"preference","flags":"flags","enumservice":"enumService","regexp":"regExp","usertag":"userTag","domain":"domain","spcode":"spCode","processType":"processType","gateway":"gateway","service":"service","domaintag":"domainTag","replacement":"replacement"}}}}';
    const builder = new EnumProfileActivationBuilder();
    const message = builder
      .setHeader('DEMO1', 'DEMO2')
      .setDossierId('DEMO1-123456')
      .setCurrentNetworkOperator("currentNetworkOperator")
      .setTypeOfNumber("typeOfNumber")
      .setScope("scope")
      .setProfileId("profileId")
      .setTtl("ttl")
      .setDnsClass("dnsClass")
      .setRecType("recType")
      .setOrder("order")
      .setPreference("preference")
      .setFlags("flags")
      .setEnumService("enumService")
      .setRegExp("regExp")
      .setUserTag("userTag")
      .setDomain("domain")
      .setSpCode("spCode")
      .setProcessType("processType")
      .setGateway("gateway")
      .setService("service")
      .setDomainTag("domainTag")
      .setReplacement("replacement")
      .build();

    expect(message.getMessageType()).to.equal(MessageTypeEnum.enumprofileactivation);
    expect(JSON.stringify(message.getMessage())).to.equal(result);
  });
});
