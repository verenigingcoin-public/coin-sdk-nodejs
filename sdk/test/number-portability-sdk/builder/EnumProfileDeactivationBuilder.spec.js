import expect from './common/expect';

import EnumProfileDeactivationBuilder from '../../../src/number-portability-sdk/messages/v3/builder/EnumProfileDeactivationBuilder';
import MessageTypeEnum from '../../../src/number-portability-sdk/messages/v3/messagetype';

describe('EnumProfileDeactivationBuilder', () => {
  xit('message should be of type enumprofiledeactivation', () => {
    const result = '{"message":{"header":{"receiver":{"networkoperator":"DEMO2"},"sender":{"networkoperator":"DEMO1"}},"body":{"enumprofiledeactivation":{"dossierid":"DEMO1-123456","currentnetworkoperator":"DEMO2","typeofnumber":"X","profileid":"X"}}}}';
    const builder = new EnumProfileDeactivationBuilder();
    const message = builder
      .setHeader('DEMO1', 'DEMO2')
      .setDossierId('DEMO1-123456')
      .setCurrentNetworkOperator('DEMO2')
      .setTypeOfNumber('X')
      .setProfileId('X')
      .build();

    expect(message.getMessageType()).to.equal(MessageTypeEnum.enumprofiledeactivation);
    expect(JSON.stringify(message.getMessage())).to.equal(result);
  });
});
