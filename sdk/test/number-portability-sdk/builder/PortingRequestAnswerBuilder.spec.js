import expect from './common/expect';

import PortingRequestAnswerBuilder from '../../../src/number-portability-sdk/messages/v3/builder/PortingRequestAnswerBuilder';
import MessageTypeEnum from '../../../src/number-portability-sdk/messages/v3/messagetype';

describe('PortingRequestAnswerBuilder', () => {
  xit('message should be of type portingrequestanswer', () => {
    const result = '{"message":{"header":{"receiver":{"networkoperator":"DEMO2"},"sender":{"networkoperator":"DEMO1"}},"body":{"portingrequestanswer":{"blocking":"N","repeats":[{"seq":{"numberseries":{"start":"06123456799","end":"06123456799"},"blockingcode":"0","firstpossibledate":"X","note":"Just a note","donorNetworkOperator":"DEMO2","donorServiceProvider":"DEMO1"}}]}}}}';
    const builder = new PortingRequestAnswerBuilder();
    const message = builder
      .setHeader('DEMO1', 'DEMO2')
      .setBlocking("N")
      .addPortingRequestAnswerSequence()
      .setNumberSeries('06123456799', '06123456799')
      .setBlockingCode("0")
      .setFirstPossibleDate("X")
      .setNote("Just a note")
      .setDonorNetworkOperator("DEMO2")
      .setDonorServiceProvider("DEMO1")
      .finish()
      .build();

    expect(message.getMessageType()).to.equal(MessageTypeEnum.portingrequestanswer);
    expect(JSON.stringify(message.getMessage())).to.equal(result);
  });
});
