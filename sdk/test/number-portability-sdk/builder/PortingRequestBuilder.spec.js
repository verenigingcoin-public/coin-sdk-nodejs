import expect from './common/expect';

import PortingRequestBuilder from '../../../src/number-portability-sdk/messages/v3/builder/PortingRequestBuilder';
import MessageTypeEnum from '../../../src/number-portability-sdk/messages/v3/messagetype';

import results from './results/PortingRequestMessage';
import buildMessage from './messages/PortingRequestMessage';
import {removeUndefined} from "../../common-sdk/utils";

function fixResult(result, actual) {
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('PortingRequestBuilder', () => {
  it('message should be of type portingrequest', () => {
    const offset = 0;
    const builder = new PortingRequestBuilder();
    const message = buildMessage(offset, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.portingrequest);
  });

  it('message should have correct structure', () => {
    const offset = 0;
    const builder = new PortingRequestBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = removeUndefined(message.getMessage());

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
