import expect from './common/expect';

import TariffChangeServiceNumberBuilder from '../../../src/number-portability-sdk/messages/v3/builder/TariffChangeServiceNumberBuilder';
import MessageTypeEnum from '../../../src/number-portability-sdk/messages/v3/messagetype';

import results from './results/TariffChangeServiceNumberMessage';
import buildMessage from './messages/TariffChangeServiceNumberMessage';
import {removeUndefined} from "../../common-sdk/utils";

function fixResult(result, actual) {
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('TariffChangeServiceNumberBuilder', () => {
  it('message should be of type tariffchangesn', () => {
    const offset = 0;
    const builder = new TariffChangeServiceNumberBuilder();
    const message = buildMessage(offset, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.tariffchangesn);
  });

  it('message should have correct structure', () => {
    const offset = 0;
    const builder = new TariffChangeServiceNumberBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = removeUndefined(message.getMessage());

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
