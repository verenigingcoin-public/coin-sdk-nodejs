/* eslint-disable-next-line node/no-unpublished-require */
import chai from 'chai';
/* eslint-disable-next-line node/no-unpublished-require */
import deepEqualInAnyOrder from 'deep-equal-in-any-order';
chai.use(deepEqualInAnyOrder);
const { expect } = chai;

export default expect;
