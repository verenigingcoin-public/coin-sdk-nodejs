export default function buildMessage(offset, builder) {
  let message;
  switch (offset) {
    case 0:
      message = builder
        .setHeader("DEMO1", "DEMO2")
        .setDossierId("DEMO1-123456")
        .setPlatformProvider("DEMO1")
        .setPlannedDateTime((new Date()).toJSON().replace(/[T:-]/g, '').split('.')[0])
        .setNote("Example note")
        .addActivationServiceNumberSequence()
        .setNumberSeries("0123456789", "0987654321")
        .setTariffinfo("2023,50", "1023,50", "0", "2", "1")
        .setPop("pop")
        .finish()
        .build();
      break;
    default:
      console.error(`buildMessage: invalid offset=${offset}`);
      break;
  }
  return message;
}

