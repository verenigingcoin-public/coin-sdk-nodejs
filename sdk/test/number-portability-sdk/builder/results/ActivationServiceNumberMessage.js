export default [{
  /* 0 */
  "message": {
    "header": {
      "receiver": { "networkoperator": "DEMO2" },
      "sender": { "networkoperator": "DEMO1" } ,
      },
    "body": {
      "activationsn": {
        "platformprovider": "DEMO1",
        "repeats": [{
          "seq": {
            "numberseries": { "start": "0123456789", "end": "0987654321" },
            "tariffinfo": { "peak": "2023,50", "offpeak": "1023,50", "currency": "0", "type": "2", "vat": "1" },
            "pop": "pop"
          }
        }],
        "dossierid": "DEMO1-123456",
        "planneddatetime": "20190710075035",
        "note": "Example note"
      }
    }
  }
}];
