const today = new Date();
const timestamp = today.getFullYear() +
  ("0" + (today.getMonth() + 1)).slice(-2) +
  ("0" + today.getDate()).slice(-2) +
  ("0" + today.getHours() + 1 ).slice(-2) +
  ("0" + today.getMinutes()).slice(-2) +
  ("0" + today.getSeconds()).slice(-2);

export default [
  /* 0 */
  {
    "message": {
      "header": {
        "receiver": {
          "networkoperator": "DEMO2"
        },
        "sender": {
          "networkoperator": "DEMO1"
        },
        "timestamp": timestamp
      },
      "body": {
        "deactivationsn": {
          "dossierid": "DEMO1-123456",
          "platformprovider": "DEMO1",
          "planneddatetime": "20190101120000",
          "repeats": [
            {
              "seq": {
                "numberseries": {
                  "start": "0123456789",
                  "end": "0987654321"
                },
                "pop": "pop"
              }
            }
          ],
        }
      }
    }
  }
];
