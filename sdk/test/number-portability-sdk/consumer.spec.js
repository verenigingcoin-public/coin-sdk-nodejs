import utils from '../../src/common-sdk/utils';
import NumberPortabilityMessageConsumer from '../../src/number-portability-sdk/messages/v3/api/consumer';
import chai from 'chai';
const { expect } = chai;

import {baseUrl, encryptedHmacSecretFile, privateKeyFile} from "../index";
const consumerName = "loadtest-loada";
const validPeriodInSeconds = 30;

describe('consumer', () => {
  it('should receive all messages', (done) => {
    const consumer = new NumberPortabilityMessageConsumer(consumerName, privateKeyFile, encryptedHmacSecretFile, baseUrl, 5, 3, validPeriodInSeconds);
    const msgs = {};
    const deferred = new utils.Deferred();
    const recordMessageArrived = (message) => {
      msgs[message.type] = message;
      // when we have received as many unique messages as there are handlers, we are done
      if (Object.keys(msgs).length === Object.keys(handlers).length)
        deferred.resolve();
    };
    const handlers = {
      onActivationsn: recordMessageArrived,
      onCancel: recordMessageArrived,
      onDeactivation: recordMessageArrived,
      onDeactivationsn: recordMessageArrived,
      onEnumActivationNumber: recordMessageArrived,
      onEnumActivationOperator: recordMessageArrived,
      onEnumActivationRange: recordMessageArrived,
      onEnumDeactivationNumber: recordMessageArrived,
      onEnumDeactivationOperator: recordMessageArrived,
      onEnumDeactivationRange: recordMessageArrived,
      onEnumProfileActivation: recordMessageArrived,
      onEnumProfileDeactivation: recordMessageArrived,
      // onErrorFound: recordMessageArrived,
      onPortingPerformed: recordMessageArrived,
      onPortingRequest: recordMessageArrived,
      onPortingRequestAnswer: recordMessageArrived,
      onPortingRequestAnswerDelayed: recordMessageArrived,
      onRangeActivation: recordMessageArrived,
      onRangeDeactivation: recordMessageArrived,
      onTariffChangeSn: recordMessageArrived,
    };
    const disposable = consumer.startConsuming(handlers);
    deferred.promise.then(() => {
      // clear the dead-man-switch function
      clearTimeout(timer);
      disposable.close();
      done();
    });
    // here setup a dead-man-switch function
    // we expect this never to be called unless at least one message doesn't arrive
    const timer = setTimeout(()=>{
      disposable.close();
      expect(Object.keys(msgs)).to.equal(Object.keys(handlers).map(h => h.toLowerCase()+'-v3'));
    }, 20000);
  }).timeout(60000);
});
