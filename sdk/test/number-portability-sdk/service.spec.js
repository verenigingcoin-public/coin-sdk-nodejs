/* eslint-disable-next-line node/no-unpublished-require */
import { use, expect } from 'chai';
/* eslint-disable-next-line node/no-unpublished-require */
import deepEqualInAnyOrder from 'deep-equal-in-any-order';
import chaiAsPromised from 'chai-as-promised';
import NumberPortabilityService from '../../src/number-portability-sdk/messages/v3/api/service';
import PortingRequestBuilder from '../../src/number-portability-sdk/messages/v3/builder/PortingRequestBuilder';
import PortingRequestAnswerBuilder from '../../src/number-portability-sdk/messages/v3/builder/PortingRequestAnswerBuilder';
import PortingPerformedBuilder from '../../src/number-portability-sdk/messages/v3/builder/PortingPerformedBuilder';
import {baseUrl, encryptedHmacSecretFile, privateKeyFile} from "../index";

use(deepEqualInAnyOrder);
use(chaiAsPromised);

const dossierId = 'DEMO1-123456';

const consumerName = "loadtest-loada";
const validPeriodInSeconds = 30;

const senderNetworkOperator = 'DEMO1';
const senderServiceProvider = senderNetworkOperator;
const receiverNetworkOperator = 'DEMO2';
const receiverServiceProvider = receiverNetworkOperator;

const startNumberRange = "0612345678";
const endNumberRange = "0612345678";
const regExpTransactionId = /^[0-9a-z]{32}$/;

describe('service', () => {

  const service = new NumberPortabilityService(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds, baseUrl);

  it('should send a PortingRequest message and receive a transactionId', (done) => {
    const builder = new PortingRequestBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setRecipientNetworkOperator(senderNetworkOperator)
      .addPortingRequestSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    const result = service.sendMessage(message);
    result.catch(done);
    result.then(obj => {
      expect(obj.transactionId).to.match(regExpTransactionId);
      done();
    }).catch(done);
  });

  it('should send a PortingRequest message with the contract field set', (done) => {
    const builder = new PortingRequestBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setContract("EARLY_TERMINATION")
      .setRecipientNetworkOperator(senderNetworkOperator)
      .addPortingRequestSequence()
      .setNumberSeries(startNumberRange, endNumberRange)
      .finish()
      .build();

    const result = service.sendMessage(message);
    result.catch(done);
    result.then(obj => {
      expect(obj.transactionId).to.match(regExpTransactionId);
      done();
    }).catch(done);
  });

  it('should fail to send a PortingRequest with an incorrect contract field set', (done) => {
    const builder = new PortingRequestBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setContract("ET")
      .setRecipientNetworkOperator(senderNetworkOperator)
      .addPortingRequestSequence()
      .setNumberSeries(startNumberRange, endNumberRange)
      .finish()
      .build();

    const result = service.sendMessage(message);
    result.catch(({response}) => {
      expect(response.statusCode).to.eq(400);
      done();
    }).catch(done);
  });

  it('should send a PortingRequestAnswer message and receive a transactionId', (done) => {
    const builder = new PortingRequestAnswerBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setBlocking("N")
      .addPortingRequestAnswerSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    const result = service.sendMessage(message);
    result.catch(done);
    result.then(obj=> {
      expect(obj.transactionId).to.match(regExpTransactionId);
      done();
    }).catch(done);
  });

  it('should send a PortingPerformed message and receive a transactionId', (done) => {
    const builder = new PortingPerformedBuilder();
    const message = builder
      .setFullHeader(senderNetworkOperator, senderServiceProvider, receiverNetworkOperator, receiverServiceProvider)
      .setDossierId(dossierId)
      .setRecipientNetworkOperator(senderNetworkOperator)
      .setDonorNetworkOperator(receiverNetworkOperator)
      .addPortingPerformedSequence()
        .setNumberSeries(startNumberRange, endNumberRange)
        .finish()
      .build();

    const result = service.sendMessage(message);
    result.catch(done);
    result.then(obj => {
      expect(obj.transactionId).to.match(regExpTransactionId);
      done();
    }).catch(done);
  });

  it('should send a confirmation message', (done) => {
    const req = service.sendConfirmation('1234');
    req.catch(done);
    req.then(() => {
      done();
    });
  });
});
